﻿CREATE TABLE [dbo].[Termini] (
    [IdTermin]              INT           IDENTITY (1, 1) NOT NULL,
    [VrijemeZauzecaPocetak] DATETIME2 (7) NOT NULL,
    [VrijemeZauzecaKraj]    DATETIME2 (7) NOT NULL,
    [DaniUNedelji]          NVARCHAR (20) NOT NULL,
    [TipNastave]            NVARCHAR (20) NOT NULL,
    [ZaduzeniPredavacId]    INT           NOT NULL,
    [UstanovaId]            INT           NOT NULL,
    [UcionicaId]            INT           NOT NULL,
    [Active]                BIT           NOT NULL,
    PRIMARY KEY CLUSTERED ([IdTermin] ASC),
    CONSTRAINT [fk_Termin_Ucionica] FOREIGN KEY ([UcionicaId]) REFERENCES [dbo].[Ucionice] ([IdUcionica]),
    CONSTRAINT [fk_Termin_Ustanova] FOREIGN KEY ([UstanovaId]) REFERENCES [dbo].[Ustanove] ([IdUstanova]),
    CONSTRAINT [fk_Termini_Korisnik] FOREIGN KEY ([ZaduzeniPredavacId]) REFERENCES [dbo].[Korisnici] ([IdKorisnik])
);

