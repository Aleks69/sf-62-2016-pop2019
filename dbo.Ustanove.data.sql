﻿SET IDENTITY_INSERT [dbo].[Ustanove] ON
INSERT INTO [dbo].[Ustanove] ([IdUstanova], [Naziv], [Lokacija], [Active], [MaksimalanBrojUcionica]) VALUES (1, N'Jugodrvo', N'Bulevar Oslobodjenja 23', 1, 25)
INSERT INTO [dbo].[Ustanove] ([IdUstanova], [Naziv], [Lokacija], [Active], [MaksimalanBrojUcionica]) VALUES (2, N'FTN', N'Ulica Gdje Su Fakulteti 223', 1, 40)
INSERT INTO [dbo].[Ustanove] ([IdUstanova], [Naziv], [Lokacija], [Active], [MaksimalanBrojUcionica]) VALUES (3, N'Park Citya', N'Narodnog Fronta 691', 1, 15)
SET IDENTITY_INSERT [dbo].[Ustanove] OFF
