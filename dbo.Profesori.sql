﻿CREATE TABLE [dbo].[Profesori] (
    [IdProfesor]           INT NOT NULL,
    [DodjeljeniAsistentId] INT NULL,
    [UstanovaId]           INT NULL,
    PRIMARY KEY CLUSTERED ([IdProfesor] ASC),
    CONSTRAINT [fk_korisnici_profesori] FOREIGN KEY ([IdProfesor]) REFERENCES [dbo].[Korisnici] ([IdKorisnik]),
    CONSTRAINT [fk_profesor_ustanova] FOREIGN KEY ([UstanovaId]) REFERENCES [dbo].[Ustanove] ([IdUstanova])
);

