﻿CREATE TABLE [dbo].[Raspored] (
    [IdRaspored] INT IDENTITY (1, 1) NOT NULL,
    [UstanovaId] INT NOT NULL,
    [TerminId]   INT NOT NULL,
    [UcionicaId] INT NOT NULL,
    [KorisnikId] INT NOT NULL,
    [Active]     BIT NOT NULL,
    PRIMARY KEY CLUSTERED ([IdRaspored] ASC),
    CONSTRAINT [fk_raspored_ustanova] FOREIGN KEY ([UstanovaId]) REFERENCES [dbo].[Ustanove] ([IdUstanova]),
    CONSTRAINT [fk_raspored_korisnik] FOREIGN KEY ([KorisnikId]) REFERENCES [dbo].[Korisnici] ([IdKorisnik]),
    CONSTRAINT [fk_raspored_ucionica] FOREIGN KEY ([UcionicaId]) REFERENCES [dbo].[Ucionice] ([IdUcionica]),
    CONSTRAINT [fk_raspored_termin] FOREIGN KEY ([TerminId]) REFERENCES [dbo].[Termini] ([IdTermin])
);

