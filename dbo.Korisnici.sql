﻿CREATE TABLE [dbo].[Korisnici] (
    [IdKorisnik]    INT           IDENTITY (1, 1) NOT NULL,
    [Ime]           NVARCHAR (25) NOT NULL,
    [Prezime]       NVARCHAR (25) NOT NULL,
    [KorisnickoIme] NVARCHAR (50) NOT NULL,
    [Email]         NVARCHAR (30) NOT NULL,
    [TipKorisnika]  NVARCHAR (15) NOT NULL,
    [Lozinka]       NVARCHAR (50) NOT NULL,
    [Active]        BIT           NOT NULL,
    CONSTRAINT [PK_Korisnici] PRIMARY KEY CLUSTERED ([IdKorisnik] ASC),
    UNIQUE NONCLUSTERED ([KorisnickoIme] ASC)
);

