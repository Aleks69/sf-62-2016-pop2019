﻿CREATE TABLE [dbo].[Asistenti] (
    [IdAsistent]         INT NOT NULL,
    [DodjeljeniProfesor] INT NULL,
    [UstanovaId]         INT NULL,
    PRIMARY KEY CLUSTERED ([IdAsistent] ASC),
    CONSTRAINT [fk_korisnici_asistenti] FOREIGN KEY ([IdAsistent]) REFERENCES [dbo].[Korisnici] ([IdKorisnik]),
    CONSTRAINT [fk_dodjeljeni_profesor] FOREIGN KEY ([DodjeljeniProfesor]) REFERENCES [dbo].[Profesori] ([IdProfesor]),
    CONSTRAINT [fk_asistent_ustanova] FOREIGN KEY ([UstanovaId]) REFERENCES [dbo].[Ustanove] ([IdUstanova])
);

