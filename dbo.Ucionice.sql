﻿CREATE TABLE [dbo].[Ucionice] (
    [IdUcionica]             INT           IDENTITY (1, 1) NOT NULL,
    [BrojUcionice]           NCHAR (10)    NULL,
    [BrojMjesta]             INT           NULL,
    [TipUcionice]            NVARCHAR (50) NULL,
    [UstanovaGdjeSeNalaziId] INT           NULL,
    [Active]                 BIT           NULL,
    PRIMARY KEY CLUSTERED ([IdUcionica] ASC),
    CONSTRAINT [fk_ucionica_ustanova] FOREIGN KEY ([UstanovaGdjeSeNalaziId]) REFERENCES [dbo].[Ustanove] ([IdUstanova])
);

