﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SF62_2016_POP2019.Entiteti;
using SF62_2016_POP2019.Logika;
using SF62_2016_POP2019.ZaOtklanjanje;

namespace SF62_2016_POP2019
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static string CONNECTION_STRING =
            @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=master;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        public MainWindow()
        {
            InitializeComponent();
            OsvjeziProzor();

        }

        private void OsvjeziProzor()
        {
            comboBox.ItemsSource = new List<ETipKorisnika>() { ETipKorisnika.ASISTENT, ETipKorisnika.PROFESOR, ETipKorisnika.ADMIN };
            PodaciUstanova.UcitajUstanove();
            PodaciUcionica.UcitajUcionice();
            PodaciUcionica.DodajUcioniceOdredjenojUstnaovi();
           // PodaciUstanova.IzbrojUcionice();
            // PodaciUstanova.DodajUstanove();
            PodaciKorisnik.UcitajKorisnike();
            //     PodaciUstanova.DodajZaposlenaLica();
            PodaciTermin.UcitajTermine();
            PodaciRaspored.UcitajRaspored();
                

            /*            foreach (var kor in PodaciKorisnik.listaKorisnika)
                        {
                            kor.Active = true;
                        }*/
        }

        private void buttonLogin_Click(object sender, RoutedEventArgs e)
        {
      

                if (!(String.IsNullOrEmpty(textBoxKorIme.Text)) && !(String.IsNullOrEmpty(passwordBox.Password)) && comboBox.SelectedIndex >= 0)
                {

                    bool logovanjeUspjesno = PodaciKorisnik.ValidirajKorisnika(textBoxKorIme.Text, passwordBox.Password, (ETipKorisnika)comboBox.SelectedItem);

                    if (logovanjeUspjesno == true)
                    {
                        Korisnik kor = PodaciKorisnik.PretraziPoKorImenu(textBoxKorIme.Text);

                        if (comboBox.SelectedItem.Equals(ETipKorisnika.ADMIN))
                        {
                            PodaciKorisnik.AktivniKorisnik = new Administrator();
                            PodaciKorisnik.AktivniKorisnik = kor as Administrator;
                            MessageBox.Show("Admin logovanje Uspjesno", "Uspjeh");
                            ProzorIzbor prozorIzbor = new ProzorIzbor();
                            prozorIzbor.ShowDialog();

                        }
                        else if (comboBox.SelectedItem.Equals(ETipKorisnika.PROFESOR))
                        {

                            PodaciKorisnik.AktivniKorisnik = new Profesor();
                            PodaciKorisnik.AktivniKorisnik = kor as Profesor;
                            MessageBox.Show("Profesor logovanje Uspjesno", "Uspjeh");
                            ProfesorAsistentProzor profesorAsistentProzor = new ProfesorAsistentProzor(kor);
                            profesorAsistentProzor.ShowDialog();

                        }
                        else if (comboBox.SelectedItem.Equals(ETipKorisnika.ASISTENT))
                        {
                            PodaciKorisnik.AktivniKorisnik = new Asistent();
                            PodaciKorisnik.AktivniKorisnik = kor as Asistent;
                            MessageBox.Show($"Asistent logovanje Uspjesno", "Uspjeh");
                            ProfesorAsistentProzor asistprozor = new ProfesorAsistentProzor(kor);
                            //AsistentProzor asistprozor = new AsistentProzor(kor);
                            asistprozor.ShowDialog();

                        }
                    }

                    else
                {
                    MessageBox.Show("Unijeli ste pogresne podatke", "Greska", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

            }
            else if (String.IsNullOrEmpty(textBoxKorIme.Text) || String.IsNullOrWhiteSpace(passwordBox.Password) || comboBox.SelectedIndex == -1)
            {
                MessageBox.Show("Niste popunili sva polja", "Greska", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void labelNastaviKaoGost_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            AdminProzorUstanova ustanovaProzor = new AdminProzorUstanova();
            ustanovaProzor.ShowDialog();
        }

        private void textBlock_MouseDown(object sender, MouseButtonEventArgs e)
        {
            AdminProzorUstanova ustanovaProzor = new AdminProzorUstanova();

            ustanovaProzor.ShowDialog();
        }
    }
}
