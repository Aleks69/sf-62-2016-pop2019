﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Controls;

namespace SF62_2016_POP2019.ValidacionaPravila
{
    class EmailValidacija : ValidationRule
    {
        public static Regex regex = new Regex(@"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*"
                                + "@"
                                + @"((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$", RegexOptions.IgnoreCase);

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            string s = value as string;

            if (s == null || String.IsNullOrWhiteSpace(s))
            {
                return new ValidationResult(false, "Ovo polje je obavezno!");
            }
            else if (regex.Match(s).Success)
            {
                return new ValidationResult(true, null);
            }
            else
            {
                return new ValidationResult(false, "Pogresan Format!");
            }
        }
    }
}
