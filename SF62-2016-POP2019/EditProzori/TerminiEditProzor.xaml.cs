﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SF62_2016_POP2019.Entiteti;
using SF62_2016_POP2019.Logika;

namespace SF62_2016_POP2019
{
    /// <summary>
    /// Interaction logic for TerminiEditProzor.xaml
    /// </summary>
    public partial class TerminiEditProzor : Window
    {
        enum Status
        {
            ADD,
            EDIT
        }

        private Status _status;
        private Termin selectedTermin;
        private List<Korisnik> listaPredavaca;
        private List<Ucionica> ucioniceUOdabranojUstanovi;
        private Termin selectedTerminCopy;
        private List<Termin> listaTerminaCopy;

        public TerminiEditProzor(Termin termin)
        {
            InitializeComponent();

            OsvjeziProzor();

            listaTerminaCopy = new List<Termin>();
            listaTerminaCopy.AddRange(PodaciTermin.listaTermina);

            if (termin.IdTermin.Equals(0))
                this._status = Status.ADD;
            else
            {
                this._status = Status.EDIT;
            }

            selectedTermin = termin;
            this.DataContext = termin;

            selectedTerminCopy = selectedTermin.Clone();
        }

        public void OsvjeziProzor()
        {
            listaPredavaca = new List<Korisnik>();

            comboBoxDanUNed.ItemsSource = new List<EDaniUnedelji>()
            {
                EDaniUnedelji.PONEDELJAK, EDaniUnedelji.UTORAK,
                EDaniUnedelji.SRIJEDA, EDaniUnedelji.CETVRTAK, EDaniUnedelji.PETAK,
                EDaniUnedelji.SUBOTA, EDaniUnedelji.NEDELJA
            };

            comboBoxTipNastave.ItemsSource = new List<ETipNastave>() { ETipNastave.Predavanja, ETipNastave.Vjezbe };

            if (PodaciKorisnik.AktivniKorisnik.TipKorisnika.Equals(ETipKorisnika.ADMIN))
            {
                comboBoxUstanova.ItemsSource = PodaciUstanova.listaUstanova;


                foreach (var predavac in PodaciKorisnik.listaKorisnika)
                {
                    if (predavac.TipKorisnika.Equals(ETipKorisnika.PROFESOR) ||
                        predavac.TipKorisnika.Equals(ETipKorisnika.ASISTENT))
                        listaPredavaca.Add(predavac);
                }

                comboBoxZaduzenaOsoba.ItemsSource = listaPredavaca;
            }

            else if (PodaciKorisnik.AktivniKorisnik.TipKorisnika.Equals(ETipKorisnika.PROFESOR))
            {
                Profesor profesor = PodaciKorisnik.AktivniKorisnik as Profesor;
                comboBoxZaduzenaOsoba.ItemsSource = new List<Profesor> { profesor };
                comboBoxZaduzenaOsoba.SelectedIndex = 0;
                comboBoxUstanova.ItemsSource = new List<Ustanova> { profesor.UstanovaZaposlenja };
                comboBoxUstanova.SelectedIndex = 0;
            }
            else if (PodaciKorisnik.AktivniKorisnik.TipKorisnika.Equals(ETipKorisnika.ASISTENT))
            {
                Asistent asistent = PodaciKorisnik.AktivniKorisnik as Asistent;
                comboBoxZaduzenaOsoba.ItemsSource = new List<Asistent> { asistent };
                comboBoxZaduzenaOsoba.SelectedIndex = 0;
                comboBoxUstanova.ItemsSource = new List<Ustanova> { asistent.UstanovaZaposlenja };
                comboBoxUstanova.SelectedIndex = 0;
            }

            if (!(comboBoxUstanova.SelectedIndex == -1))
            {
                ucioniceUOdabranojUstanovi = new List<Ucionica>();

                Ustanova ustanova = comboBoxUstanova.SelectedItem as Ustanova;

                int indeks = PodaciUstanova.listaUstanova.IndexOf(PodaciUstanova.listaUstanova
                    .Where(u => u.SifraUstanove.Equals(ustanova.SifraUstanove)).FirstOrDefault());

                foreach (var ucionica in PodaciUstanova.listaUstanova[indeks].ListaUcionica)
                {
                    ucioniceUOdabranojUstanovi.Add(ucionica);
                }

                comboBoxUcionica.ItemsSource = ucioniceUOdabranojUstanovi;
            }
        }

        private void buttonOk_Click(object sender, RoutedEventArgs e)
        {
            /*Termin termin = PodaciTermin.PretraziPoIdu(Convert.ToInt32(textBoxSifra.Text));


            if (termin != null && _status.Equals(Status.ADD))
            {
                MessageBox.Show($"Termin sa sifrom {termin.IdTermin} vec postoji", "Upozorenje",
                    MessageBoxButton.OK);
                return;
            }*/

            if (comboBoxDanUNed.SelectedIndex < 0 || comboBoxTipNastave.SelectedIndex < 0 ||
                comboBoxUcionica.SelectedIndex < 0 || comboBoxUstanova.SelectedIndex < 0 ||
                comboBoxZaduzenaOsoba.SelectedIndex < 0)
            {
                MessageBox.Show("Niste popunili sva polja!", "Greska", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (!(ValidacionaPravila.TerminValidacija.regex.IsMatch(TextBoxVrjiemePocetka
                    .Text)) || !(ValidacionaPravila.TerminValidacija.regex.IsMatch(TextBoxVrjiemeKraja.Text)))
            {
                MessageBox.Show("Format za vrijeme staviti kao hh:mm AM/PM", "Greska", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (_status.Equals(Status.ADD))
            {
                DateTime pocetakRadnogVremena =
                    new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 07, 00, 00);
                DateTime krajRadnogVremena =
                    new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 20, 00, 00);

                DateTime vrijemeOd = Convert.ToDateTime(TextBoxVrjiemePocetka.Text);
                DateTime vrijemeDo = Convert.ToDateTime(TextBoxVrjiemeKraja.Text);

                if (vrijemeOd.TimeOfDay >= pocetakRadnogVremena.TimeOfDay &&
                    vrijemeDo.TimeOfDay <= krajRadnogVremena.TimeOfDay)
                {
                    if ((comboBoxDanUNed.SelectedItem.ToString().ToLower() ==
                         EDaniUnedelji.SUBOTA.ToString().ToLower() && checkBoxVanredno.IsChecked == false)
                        || (comboBoxDanUNed.SelectedItem.ToString().ToLower() ==
                            EDaniUnedelji.NEDELJA.ToString().ToLower() && checkBoxVanredno.IsChecked == false))
                    {
                        MessageBox.Show(
                            "Ukoliko zelite da zauzmete termin subotom ili nedeljom, obiljezite polje 'Vanredni Termin' ",
                            "Upozorenje", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }

                    int result = DateTime.Compare(vrijemeOd, vrijemeDo);

                    TimeSpan vrijemeTrajanjaTermina = PodaciTermin.IzracunajTrajanje(vrijemeDo, vrijemeOd);



                    foreach (var t in PodaciTermin.listaTermina)
                    {


                        if (t.Active.Equals(true) && vrijemeOd.Ticks >= t.VrijemeZauzecaPocetak.Ticks && 
                            vrijemeDo.Ticks <= t.VrijemeZauzecaKraj.Ticks &&
                            t.DaniUNedelji.ToString().ToLower() == comboBoxDanUNed.SelectedValue.ToString().ToLower()
                            && vrijemeOd.Ticks < vrijemeDo.Ticks)
                        {
                            MessageBox.Show("Termin u tom vremenskom razdoblju vec postoji", "Greska",
                                MessageBoxButton.OK, MessageBoxImage.Error);
                            return;
                        }

                        if (t.Active.Equals(true) && comboBoxDanUNed.SelectedItem.ToString().ToLower() == t.DaniUNedelji.ToString().ToLower() &&
                            vrijemeOd.Ticks <= t.VrijemeZauzecaPocetak.Ticks &&
                            vrijemeDo.Ticks >= t.VrijemeZauzecaPocetak.Ticks &&
                             vrijemeDo.Ticks <= t.VrijemeZauzecaKraj.Ticks)
                        {
                            MessageBox.Show("Termin u tom vremenskom razdoblju vec postoji", "Greska",
                                MessageBoxButton.OK, MessageBoxImage.Error);
                            return;
                        }

                        if (t.Active.Equals(true) && comboBoxDanUNed.SelectedItem.ToString().ToLower() == t.DaniUNedelji.ToString().ToLower() &&
                            vrijemeOd.Ticks >= t.VrijemeZauzecaPocetak.Ticks &&
                            vrijemeOd.Ticks <= t.VrijemeZauzecaKraj.Ticks &&
                            vrijemeDo.Ticks >= t.VrijemeZauzecaKraj.Ticks)
                        {
                            MessageBox.Show("Termin u tom vremenskom razdoblju vec postoji", "Greska",
                                MessageBoxButton.OK, MessageBoxImage.Error);
                            return;
                        }

                        if (t.Active.Equals(true) && comboBoxDanUNed.SelectedItem.ToString().ToLower() == t.DaniUNedelji.ToString().ToLower() &&
                            vrijemeOd.Ticks <= t.VrijemeZauzecaPocetak.Ticks &&
                            vrijemeDo.Ticks >= t.VrijemeZauzecaKraj.Ticks)
                        {
                            MessageBox.Show("Termin u tom vremenskom razdoblju vec postoji", "Greska",
                                MessageBoxButton.OK, MessageBoxImage.Error);
                            return;
                        }
                    }



                    if (result < 0)
                    {

                        BindingExpression dan = comboBoxDanUNed.GetBindingExpression(Selector.SelectedItemProperty);
                        dan.UpdateSource();


                        selectedTermin.VrijemeZauzecaPocetak = vrijemeOd;
                        selectedTermin.VrijemeZauzecaKraj = vrijemeDo;

                        Ucionica ucionica = comboBoxUcionica.SelectedValue as Ucionica;
                        selectedTermin.UcionicaId = ucionica.IdUcionice;

                        Ustanova ustanova = comboBoxUstanova.SelectedValue as Ustanova;
                        selectedTermin.UstanovaId = ustanova.SifraUstanove;

                        Korisnik predavac = comboBoxZaduzenaOsoba.SelectedValue as Korisnik;
                        selectedTermin.ZaduzeniPredavacId = predavac.Id;

                        PodaciTermin.DodajTermine(selectedTermin);
                        PodaciTermin.listaTermina.Clear();
                        PodaciTermin.UcitajTermine();

                        this.DialogResult = true;
                        this.Close();
                    }

                    else if (result > 0 || result == 0)
                    {
                        MessageBox.Show("Pocetak termina mora biti prije zavrsetka!", "Greska!", MessageBoxButton.OK,
                            MessageBoxImage.Error);
                        return;
                    }
                }

                else
                {
                    MessageBox.Show(
                        "Radno vrijeme je definisano od 07:00-20:00, molimo vas zauzmite termine u tom opsegu",
                        "Greska", MessageBoxButton.OK, MessageBoxImage.Warning);
                }

            }
            else if (_status.Equals(Status.EDIT))
            {

                Debug.WriteLine("Pocetak originalnog termina: {0}", selectedTermin.VrijemeZauzecaPocetak);
                Debug.WriteLine("Pocetak kopije: {0}", selectedTerminCopy.VrijemeZauzecaPocetak);
                Debug.WriteLine("Kraj originalnog termina: {0}", selectedTermin.VrijemeZauzecaKraj);
                Debug.WriteLine("Kraj kopije: {0}", selectedTerminCopy.VrijemeZauzecaKraj);

                DateTime pocetakRadnogVremena =
                    new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 07, 00, 00);
                DateTime KrajRadnogVremena =
                    new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 20, 00, 00);

                DateTime vrijemeOd = Convert.ToDateTime(TextBoxVrjiemePocetka.Text);
                DateTime vrijemeDo = Convert.ToDateTime(TextBoxVrjiemeKraja.Text);

                if (vrijemeOd.TimeOfDay >= pocetakRadnogVremena.TimeOfDay &&
                    vrijemeDo.TimeOfDay <= KrajRadnogVremena.TimeOfDay)
                {
                    if ((comboBoxDanUNed.SelectedItem.ToString().ToLower() ==
                         EDaniUnedelji.SUBOTA.ToString().ToLower() && checkBoxVanredno.IsChecked == false)
                        || (comboBoxDanUNed.SelectedItem.ToString().ToLower() ==
                            EDaniUnedelji.NEDELJA.ToString().ToLower() && checkBoxVanredno.IsChecked == false))
                    {
                        MessageBox.Show(
                            "Ukoliko zelite da zauzmete termin subotom ili nedeljom, obiljezite polje 'Vanredni Termin' ",
                            "Upozorenje", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }

                    int result = DateTime.Compare(vrijemeOd, vrijemeDo);

                    TimeSpan vrijemeTrajanjaTermina = PodaciTermin.IzracunajTrajanje(vrijemeDo, vrijemeOd);

                    if (result > 0 || result == 0)
                    {
                        MessageBox.Show("Pocetak termina mora biti prije zavrsetka!", "Greska!", MessageBoxButton.OK,
                            MessageBoxImage.Error);
                        return;
                    }

                    if (vrijemeOd.TimeOfDay >= selectedTerminCopy.VrijemeZauzecaPocetak.TimeOfDay &&
                        vrijemeDo.TimeOfDay <= selectedTerminCopy.VrijemeZauzecaKraj.TimeOfDay)
                    {
                        this.DialogResult = true;
                        this.Close();
                    }

                    else
                    {
                        foreach (var t in listaTerminaCopy)
                        {

                            if (t.Active.Equals(true) && vrijemeOd.Ticks >= t.VrijemeZauzecaPocetak.Ticks && vrijemeDo.Ticks <=
                                t.VrijemeZauzecaKraj.Ticks
                                && t.DaniUNedelji.ToString()
                                    .ToLower() ==
                                comboBoxDanUNed.SelectedValue
                                    .ToString()
                                    .ToLower() &&
                                vrijemeOd.Ticks < vrijemeDo.Ticks)
                            {
                                MessageBox.Show("Termin u tom vremenskom razdoblju vec postoji", "Greska",
                                    MessageBoxButton.OK, MessageBoxImage.Error);
                                return;
                            }

                            if (t.Active.Equals(true) && comboBoxDanUNed.SelectedItem.ToString().ToLower() ==
                                t.DaniUNedelji.ToString().ToLower() &&
                                vrijemeOd.Ticks <= t.VrijemeZauzecaPocetak.Ticks &&
                                (vrijemeDo.Ticks >= t.VrijemeZauzecaPocetak.Ticks &&
                                 vrijemeDo.Ticks <= t.VrijemeZauzecaKraj.Ticks))
                            {
                                MessageBox.Show("Termin u tom vremenskom razdoblju vec postoji", "Greska",
                                    MessageBoxButton.OK, MessageBoxImage.Error);
                                return;
                            }

                            if ((t.Active.Equals(true) && comboBoxDanUNed.SelectedItem.ToString().ToLower() == t.DaniUNedelji.ToString().ToLower() &&
                                 vrijemeOd.Ticks >= t.VrijemeZauzecaPocetak.Ticks &&
                                 vrijemeOd.Ticks <= t.VrijemeZauzecaKraj.Ticks) &&
                                vrijemeDo.Ticks >= t.VrijemeZauzecaKraj.Ticks)
                            {
                                MessageBox.Show("Termin u tom vremenskom razdoblju vec postoji", "Greska",
                                    MessageBoxButton.OK, MessageBoxImage.Error);
                                return;
                            }
                               if (t.Active.Equals(true) && comboBoxDanUNed.SelectedItem.ToString().ToLower() == t.DaniUNedelji.ToString().ToLower() &&
                                   vrijemeOd.Ticks <= t.VrijemeZauzecaPocetak.Ticks &&
                                   vrijemeDo.Ticks >= t.VrijemeZauzecaKraj.Ticks)
                        {
                            MessageBox.Show("4 Termin u tom vremenskom razdoblju vec postoji", "Greska",
                                MessageBoxButton.OK, MessageBoxImage.Error);
                            return;
                        }
                        }

                        if (result < 0)
                        {
                            BindingExpression pocetak =
                                TextBoxVrjiemePocetka.GetBindingExpression(TextBox.TextProperty);
                            pocetak.UpdateSource();

                            BindingExpression kraj = TextBoxVrjiemeKraja.GetBindingExpression(TextBox.TextProperty);
                            kraj.UpdateSource();

                            BindingExpression dan = comboBoxDanUNed.GetBindingExpression(Selector.SelectedItemProperty);
                            dan.UpdateSource();


                            selectedTermin.VrijemeZauzecaPocetak = vrijemeOd;
                            selectedTermin.VrijemeZauzecaKraj = vrijemeDo;

                            Ucionica ucionica = comboBoxUcionica.SelectedValue as Ucionica;
                            selectedTermin.UcionicaId = ucionica.IdUcionice;

                            Ustanova ustanova = comboBoxUstanova.SelectedValue as Ustanova;
                            selectedTermin.UstanovaId = ustanova.SifraUstanove;

                            Korisnik korisnik = comboBoxZaduzenaOsoba.SelectedValue as Korisnik;
                            /*if (korisnik.TipKorisnika.ToString().ToLower()
                                .Equals(ETipKorisnika.PROFESOR.ToString().ToLower()))
                            {
                                Profesor profa = korisnik as Profesor; ;
                        }*/

                            selectedTermin.ZaduzeniPredavacId = korisnik.Id;


                            PodaciTermin.IzmjeniTermin(selectedTermin);

                            //PodaciTermin.listaTermina.Add(selectedTermin);
                            this.DialogResult = true;
                            this.Close();
                        }

                        /*else if (result > 0 || result == 0)
                        {
                            MessageBox.Show("Pocetak termina mora biti prije zavrsetka!", "Greska!", MessageBoxButton.OK,
                                MessageBoxImage.Error);
                            return;
                        }*/
                    }




                    /*
                    if (result < 0)
                    {
                        //PodaciTermin.listaTermina.Add(selectedTermin);
                        this.DialogResult = true;
                        this.Close();
                    }

                    else if (result > 0 || result == 0)
                    {
                        MessageBox.Show("Pocetak termina mora biti prije zavrsetka!", "Greska!", MessageBoxButton.OK,
                            MessageBoxImage.Error);
                        return;
                    }*/
                }

                else
                {
                    MessageBox.Show(
                        "Radno vrijeme je definisano od 07:00-20:00, molimo vas zauzmite termine u tom opsegu",
                        "Greska", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }


        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void comboBoxUstanova_DropDownClosed_1(object sender, EventArgs e)
        {
            if (!(comboBoxUstanova.SelectedIndex == -1))
            {
                ucioniceUOdabranojUstanovi = new List<Ucionica>();

                Ustanova ustanova = comboBoxUstanova.SelectedItem as Ustanova;

                int indeks = PodaciUstanova.listaUstanova.IndexOf(PodaciUstanova.listaUstanova
                    .Where(u => u.SifraUstanove.Equals(ustanova.SifraUstanove)).FirstOrDefault());

                foreach (var ucionica in PodaciUstanova.listaUstanova[indeks].ListaUcionica)
                {
                    ucioniceUOdabranojUstanovi.Add(ucionica);
                }

                comboBoxUcionica.ItemsSource = ucioniceUOdabranojUstanovi;
            }
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            if (!(comboBoxUstanova.SelectedIndex == -1))
            {
                ucioniceUOdabranojUstanovi = new List<Ucionica>();

                Ustanova ustanova = comboBoxUstanova.SelectedItem as Ustanova;

                int indeks = PodaciUstanova.listaUstanova.IndexOf(PodaciUstanova.listaUstanova
                    .Where(u => u.SifraUstanove.Equals(ustanova.SifraUstanove)).FirstOrDefault());

                foreach (var ucionica in PodaciUstanova.listaUstanova[indeks].ListaUcionica)
                {
                    ucioniceUOdabranojUstanovi.Add(ucionica);
                }

                comboBoxUcionica.ItemsSource = ucioniceUOdabranojUstanovi;
            }
        }
    }
}
