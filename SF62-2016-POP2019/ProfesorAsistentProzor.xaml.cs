﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SF62_2016_POP2019.Entiteti;
using SF62_2016_POP2019.Logika;

namespace SF62_2016_POP2019
{
    /// <summary>
    /// Interaction logic for ProfesorAsistentProzor.xaml
    /// </summary>
    public partial class ProfesorAsistentProzor : Window
    {
        ICollectionView viewListaAsistenata;
        private ICollectionView viewRaspored;
        public static Profesor profesor;
        public static Asistent asistent;
        private ObservableCollection<Termin> rasporedZaOdabranogProfesora;

        public ProfesorAsistentProzor(Korisnik korisnik)
        {
            InitializeComponent();

            dataGrid.IsReadOnly = true;
            rasporedZaOdabranogProfesora = new ObservableCollection<Termin>();
            profesor = korisnik as Profesor;
            asistent = korisnik as Asistent;
            OsvjeziProzor();

        }

        private bool CustomFilter(object obj)
        {
            Korisnik korisnik = obj as Korisnik;
            return korisnik.Active;
        }

        public void OsvjeziProzor()
        {
            if (PodaciKorisnik.AktivniKorisnik.TipKorisnika.Equals(ETipKorisnika.ASISTENT))
            {
                tabItemAsistenti.Visibility = Visibility.Hidden;

                textBlockIme.Text = asistent.Ime;
                textBlockPrezime.Text = asistent.Prezime;
                textBlockKorIme.Text = asistent.KorisnickoIme;
                textBlockEmail.Text = asistent.Email;
                textBlockTip.Text = asistent.TipKorisnika.ToString();
            }

            if (PodaciKorisnik.AktivniKorisnik.TipKorisnika.Equals(ETipKorisnika.PROFESOR))
            {
                Profesor p = PodaciKorisnik.PretraziPoIDu(profesor.Id) as Profesor;

                viewListaAsistenata = CollectionViewSource.GetDefaultView(p.ListaAsistenata);
                dataGridListaAsistenata.ItemsSource = viewListaAsistenata;
                viewListaAsistenata.Filter = CustomFilter;

                textBlockIme.Text = profesor.Ime;
                textBlockPrezime.Text = profesor.Prezime;
                textBlockKorIme.Text = profesor.KorisnickoIme;
                textBlockEmail.Text = profesor.Email;
                textBlockTip.Text = profesor.TipKorisnika.ToString();

            }


            foreach (var raspored in PodaciTermin.listaTermina)
            {
                if (PodaciKorisnik.AktivniKorisnik.Id.Equals(raspored.ZaduzeniPredavacId))
                {
                    rasporedZaOdabranogProfesora.Add(raspored);
                }
            }

            viewRaspored = CollectionViewSource.GetDefaultView(rasporedZaOdabranogProfesora);
            dataGrid.ItemsSource = viewRaspored;

        }

        private void buttonDodaj_Click(object sender, RoutedEventArgs e)
        {
            Asistent asistent = new Asistent();

            DodajAsistentaProzor asistentProzor = new DodajAsistentaProzor(asistent);
            asistentProzor.ShowDialog();

            if (asistentProzor.DialogResult == true)
            {
                OsvjeziProzor();
                viewListaAsistenata.Refresh();

            }

        }

        private void buttonUkloni_Click(object sender, RoutedEventArgs e)
        {
            Korisnik korisnik = dataGridListaAsistenata.SelectedValue as Korisnik;
            if (korisnik != null)
            {
                Korisnik izbrisanKorisnik = PodaciKorisnik.PretraziPoKorImenu(korisnik.KorisnickoIme);
                izbrisanKorisnik.Active = false;

                int indeks = profesor.ListaAsistenata.IndexOf(profesor.ListaAsistenata.Where(u => u.KorisnickoIme.Equals(izbrisanKorisnik.KorisnickoIme)).FirstOrDefault());
                profesor.ListaAsistenata[indeks].Active = false;

              //  profesor.ListaAsistenata[indeks].DodjeljeniProfesor = null;


                viewListaAsistenata.Refresh();
            }
        }

        private void buttonTermin_Click(object sender, RoutedEventArgs e)
        {
            profesor = PodaciKorisnik.AktivniKorisnik as Profesor;
            Termin termin = new Termin();
            termin.ZaduzeniPredavac = PodaciKorisnik.AktivniKorisnik;
            termin.Ustanova = profesor.UstanovaZaposlenja;
            TerminiEditProzor terminProzor = new TerminiEditProzor(termin);
            terminProzor.ShowDialog();

            if (terminProzor.DialogResult == true)
            {
                rasporedZaOdabranogProfesora.Clear();
                OsvjeziProzor();
                viewRaspored.Refresh();
            }

        }

        private void dataGridListaAsistenata_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Error") || e.PropertyName.Equals("Active") || e.PropertyName.Equals("IdDodjeljenogProfesora") || e.PropertyName.Equals("TipKorisnika")
                || e.PropertyName.Equals("Termini") || e.PropertyName.Equals("UstanovaZaposlenjaId") || e.PropertyName.Equals("Id") || e.PropertyName.Equals("Lozinka"))
                e.Column.Visibility = Visibility.Collapsed;
        }

        private void dataGrid_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyType == typeof(System.DateTime))
                (e.Column as DataGridTextColumn).Binding.StringFormat = "HH:mm";

            if (e.PropertyName.Equals("Error") || e.PropertyName.Equals("Active") || e.PropertyName.Equals("ZaduzeniPredavacId") || e.PropertyName.Equals("UstanovaId") || e.PropertyName.Equals("UcionicaId") )
                e.Column.Visibility = Visibility.Collapsed;
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            /*dataGrid.Columns[0].Header = "Sifra";
            dataGrid.Columns[1].Header = "Pocetak Termina";
            dataGrid.Columns[2].Header = "Kraj Termina";
            dataGrid.Columns[3].Header = "Dan";
            dataGrid.Columns[4].Header = "Tip Nastave";
            dataGrid.Columns[5].Header = "Predavac";

            dataGridListaAsistenata.Columns[0].Header = "Nadlezni Profesor";
            dataGridListaAsistenata.Columns[3].Header = "Ustanova";
            dataGridListaAsistenata.Columns[8].Header = "Korisnicko Ime";
            dataGridListaAsistenata.Columns[9].Header = "E-Mail";
            dataGridListaAsistenata.Columns[10].Header = "Tip Korisnika";*/
        }

        private void tabRaspored_GotFocus(object sender, RoutedEventArgs e)
        {
            dataGrid.Columns[0].Header = "Sifra";
            dataGrid.Columns[1].Header = "Pocetak Termina";
            dataGrid.Columns[2].Header = "Kraj Termina";
            dataGrid.Columns[3].Header = "Dan";
            dataGrid.Columns[4].Header = "Tip Nastave";
            dataGrid.Columns[6].Header = "Predavac";
        }

        private void tabItemAsistenti_GotFocus(object sender, RoutedEventArgs e)
        {
            dataGridListaAsistenata.Columns[0].Header = "Nadlezni Profesor";
            dataGridListaAsistenata.Columns[3].Header = "Ustanova";
            dataGridListaAsistenata.Columns[8].Header = "Korisnicko Ime";
            dataGridListaAsistenata.Columns[9].Header = "E-Mail";
            dataGridListaAsistenata.Columns[10].Header = "Tip Korisnika";
        }
    }
}