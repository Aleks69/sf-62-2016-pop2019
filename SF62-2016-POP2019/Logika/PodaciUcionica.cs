﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using SF62_2016_POP2019.Entiteti;

namespace SF62_2016_POP2019.Logika
{
    class PodaciUcionica
    {

        public static ObservableCollection<Ucionica> listaUcionica;

        /*public static void DodajUcionice()
        {
            listaUcionica = new ObservableCollection<Ucionica>();
            listaUcionica.Add(new Ucionica("1", "A1", 50, ETipUcionice.SARACUNARIMA));
            listaUcionica.Add(new Ucionica("2", "A2", 50, ETipUcionice.SARACUNARIMA));
            listaUcionica.Add(new Ucionica("3", "A3", 30, ETipUcionice.SARACUNARIMA));
            listaUcionica.Add(new Ucionica("4", "B1", 100, ETipUcionice.BEZRACUNARA));
            listaUcionica.Add(new Ucionica("5", "B2", 30, ETipUcionice.SARACUNARIMA));
            listaUcionica.Add(new Ucionica("6", "B3", 30, ETipUcionice.SARACUNARIMA));
            listaUcionica.Add(new Ucionica("7", "C1", 100, ETipUcionice.BEZRACUNARA));
            listaUcionica.Add(new Ucionica("8", "C2", 150, ETipUcionice.BEZRACUNARA));
            listaUcionica.Add(new Ucionica("9", "C3", 30, ETipUcionice.SARACUNARIMA));
        }*/


        public static void UcitajUcionice()
        {
            listaUcionica = new ObservableCollection<Ucionica>();

            using (SqlConnection conn = new SqlConnection(MainWindow.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from ucionice";
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;

                da.Fill(ds, "Ucionice");

                foreach (DataRow row in ds.Tables["Ucionice"].Rows)
                {
                    int id = (int)row["IdUcionica"];
                    string brUcionice = (string)row["BrojUcionice"];
                    int brojMjesta = (int)row["BrojMjesta"];
                    string tipUcionice = (string)row["TipUcionice"];
                    int idUstanove = (int)row["UstanovaGdjeSeNalaziId"];
                    bool active = (bool)row["Active"];

                    if (tipUcionice.Equals("SARACUNARIMA"))
                    {
                        Ucionica ucionica = new Ucionica(id, brUcionice, brojMjesta,
                            ETipUcionice.SARACUNARIMA);
                        ucionica.IdUcionice = id;
                        ucionica.UstanovaGdjeSeNalaziId = idUstanove;


                        listaUcionica.Add(ucionica);

                    }
                    else if (tipUcionice.Equals("BEZRACUNARA"))
                    {
                        Ucionica ucionica = new Ucionica(id, brUcionice, brojMjesta,
                            ETipUcionice.BEZRACUNARA);
                        ucionica.IdUcionice = id;
                        ucionica.UstanovaGdjeSeNalaziId = idUstanove;
                        listaUcionica.Add(ucionica);

                    }

                }


                foreach (var ustanova in PodaciUstanova.listaUstanova)
                {
                    foreach (var ucionica in listaUcionica)
                    {
                        if (ucionica.UstanovaGdjeSeNalaziId.Equals(ustanova.SifraUstanove))
                        {
                            ucionica.UstanovaGdjeSeNalazi = ustanova;
                            ucionica.UstanovaGdjeSeNalaziId = ustanova.SifraUstanove;
                        }
                    }
                }

            }
        }

        public static void DodajUcioniceOdredjenojUstnaovi()
        {
            foreach (var ustanova in PodaciUstanova.listaUstanova)
            {
                foreach (var ucionica in listaUcionica)
                {
                    if (ucionica.UstanovaGdjeSeNalaziId.Equals(ustanova.SifraUstanove))
                    {
                        ustanova.ListaUcionica.Add(ucionica);

                    }

                }

            }
        }


        public static void DodajUcionicu(Ucionica u)
        {
            using (SqlConnection connection = new SqlConnection(MainWindow.CONNECTION_STRING))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();

                command.CommandText =
                    @"INSERT INTO UCIONICE(BrojUcionice,BrojMjesta,TipUcionice,UstanovaGdjeSeNalaziId,Active)
                    VALUES( @BrojUcionice, @BrojMjesta, @TipUcionice, @UstanovaId, @Active)";

               // command.Parameters.Add(new SqlParameter("@IdUcionica", u.IdUcionice));
                command.Parameters.Add(new SqlParameter("@BrojUcionice", u.BrojUcionice));
                command.Parameters.Add(new SqlParameter("@BrojMjesta", u.BrojMjesta));
                command.Parameters.Add(new SqlParameter("@TipUcionice", u.TipUcionice.ToString()));
                command.Parameters.Add(new SqlParameter("@UstanovaId", u.UstanovaGdjeSeNalaziId));
                command.Parameters.Add(new SqlParameter("@Active", u.Active));
                command.Parameters.Add(new SqlParameter("@Sifra", u.IdUcionice));

                command.ExecuteNonQuery();

            }
        }

        public static void IzmjeniUcionicu(Ucionica u)
        {
            using (SqlConnection connection = new SqlConnection(MainWindow.CONNECTION_STRING))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                command.CommandText =
                    @"UPDATE UCIONICE SET BrojUcionice=@BrojUcionice, BrojMjesta=@BrojMjesta, TipUcionice=@TipUcionice, 
            UstanovaGdjeSeNalaziId=@UstanovaId, Active=@Active WHERE IdUcionica=@IdUcionice";

                command.Parameters.Add(new SqlParameter("@IdUcionice", u.IdUcionice));
                command.Parameters.Add(new SqlParameter("@BrojUcionice", u.BrojUcionice));
                command.Parameters.Add(new SqlParameter("@BrojMjesta", u.BrojMjesta));
                command.Parameters.Add(new SqlParameter("@TipUcionice", u.TipUcionice.ToString()));
                command.Parameters.Add(new SqlParameter("@UstanovaId", u.UstanovaGdjeSeNalaziId));
                command.Parameters.Add(new SqlParameter("@Active", u.Active));
                command.Parameters.Add(new SqlParameter("@Sifra", u.IdUcionice));

                command.ExecuteNonQuery();
            }

        }

        public static void IzbrisiUcionicu(Ucionica u)
        {
            using (SqlConnection connection = new SqlConnection(MainWindow.CONNECTION_STRING))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                command.CommandText = @"UPDATE UCIONICE SET Active=0 WHERE IdUcionica=@IdUcionica";
                ;
                command.Parameters.Add(new SqlParameter("@IdUcionica", u.IdUcionice));

                command.ExecuteNonQuery();
            }
        }

        public static Ucionica PretraziPoSifri(string sifra)
        {
            foreach (var ucionica in listaUcionica)
            {
                if (sifra.Equals(ucionica.IdUcionice))
                    return ucionica;
            }

            return null;
        }

        public static Ucionica PretraziPoIdu(int id)
        {
            foreach (var ucionica in listaUcionica)
            {
                if (id.Equals(ucionica.IdUcionice))
                    return ucionica;
            }

            return null;
        }



    }
}
