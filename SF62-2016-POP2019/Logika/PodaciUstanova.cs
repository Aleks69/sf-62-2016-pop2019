﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SF62_2016_POP2019.Entiteti;

namespace SF62_2016_POP2019.Logika
{
    class PodaciUstanova
    {
        public static ObservableCollection<Ustanova> listaUstanova { get; set; }

        /*
        public static void DodajUstanove()
        {
            listaUstanova = new ObservableCollection<Ustanova>();


            listaUstanova.Add(new Ustanova("1", "Jugodrvo", "Bulevar Oslobodjenja 23"){MaksimalanBrojUcionica = 3});
            listaUstanova[0].ListaUcionica.Add(PodaciUcionica.listaUcionica[0]);
            listaUstanova[0].ListaUcionica.Add(PodaciUcionica.listaUcionica[1]);
            listaUstanova[0].ListaUcionica.Add(PodaciUcionica.listaUcionica[2]);

            listaUstanova.Add(new Ustanova("2", "FTN", "Neka Druga ulica 69") { MaksimalanBrojUcionica = 50 });
            listaUstanova[1].ListaUcionica.Add(PodaciUcionica.listaUcionica[3]);
            listaUstanova[1].ListaUcionica.Add(PodaciUcionica.listaUcionica[4]);
            listaUstanova[1].ListaUcionica.Add(PodaciUcionica.listaUcionica[5]);

            listaUstanova.Add(new Ustanova("3", "Park City", "Narodnog Fronta 23") { MaksimalanBrojUcionica = 40 });
            listaUstanova[2].ListaUcionica.Add(PodaciUcionica.listaUcionica[6]);
            listaUstanova[2].ListaUcionica.Add(PodaciUcionica.listaUcionica[7]);
            listaUstanova[2].ListaUcionica.Add(PodaciUcionica.listaUcionica[8]);


        }*/

        /*
        public static void DodajZaposlenaLica()
        {
            listaUstanova[0].ZaposenaLica.Add(PodaciKorisnik.listaKorisnika[1]); ;
            listaUstanova[0].ZaposenaLica.Add(PodaciKorisnik.listaKorisnika[3]);
            listaUstanova[1].ZaposenaLica.Add(PodaciKorisnik.listaKorisnika[2]); ;
            listaUstanova[1].ZaposenaLica.Add(PodaciKorisnik.listaKorisnika[4]);
        }
        */

        public static void UcitajUstanove()
        {
            listaUstanova = new ObservableCollection<Ustanova>();

            using(SqlConnection conn = new SqlConnection(MainWindow.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from ustanove";
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Ustanove");

                foreach (DataRow row in ds.Tables["Ustanove"].Rows)
                {
                    int id = (int) row["IdUstanova"];
                    string naziv = (string) row["Naziv"];
                    string lokacija = (string) row["Lokacija"];
                    bool active = (bool) row["Active"];
                    int maksBrUcionica = (int) row["MaksimalanBrojUcionica"];


                    Ustanova ustanova = new Ustanova(id, naziv, lokacija);
                    ustanova.MaksimalanBrojUcionica = maksBrUcionica;
                    ustanova.Active = active;
                    listaUstanova.Add(ustanova);

              
                }
            }
        }

        public static void DodajUstanovu(Ustanova u)
        {
            using (SqlConnection conn = new SqlConnection(MainWindow.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText =
                    @"INSERT INTO USTANOVE(Naziv, Lokacija, Active, MaksimalanBrojUcionica) VALUES(@Naziv, @Lokacija, @Active, @MaksBrUcionica)";

                command.Parameters.Add(new SqlParameter("@IdUstanova", u.SifraUstanove)); 
                command.Parameters.Add(new SqlParameter("@Naziv", u.Naziv)); 
                command.Parameters.Add(new SqlParameter("@Lokacija", u.Lokacija)); 
                command.Parameters.Add(new SqlParameter("@Active", u.Active));
                command.Parameters.Add(new SqlParameter("@MaksBrUcionica", u.MaksimalanBrojUcionica));

                command.ExecuteNonQuery();
            }
        }

        public static void IzmjeniUstanovu(Ustanova u)
        {
            using (SqlConnection conn = new SqlConnection(MainWindow.CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText =
                    @"UPDATE USTANOVE SET Naziv=@Naziv, Lokacija=@Lokacija, Active=@Active, MaksimalanBrojUcionica=@MaksBrUcionica WHERE IdUstanova = @IdUstanova";

                command.Parameters.Add(new SqlParameter("@IdUstanova", u.SifraUstanove)); 
                command.Parameters.Add(new SqlParameter("@Naziv", u.Naziv));
                command.Parameters.Add(new SqlParameter("@Lokacija", u.Lokacija));
                command.Parameters.Add(new SqlParameter("@Active", u.Active));
                command.Parameters.Add(new SqlParameter("@MaksBrUcionica", u.MaksimalanBrojUcionica));

                command.ExecuteNonQuery();
            }
        }

        public static void IzbrisiUstanovu(Ustanova u)
        {
            using (SqlConnection connection = new SqlConnection(MainWindow.CONNECTION_STRING))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                command.CommandText = @"UPDATE USTANOVE SET Active=0 WHERE IdUstanova=@IdUstanova";

                command.Parameters.Add(new SqlParameter("@IdUstanova", u.SifraUstanove));

                command.ExecuteNonQuery();
            }
        }

        public static Ustanova PretraziPoSifri(int sifra)
        {
            foreach(var ustanova in listaUstanova)
            {
                if (sifra.Equals(ustanova.SifraUstanove))
                    return ustanova;
            }
            return null;
        }

        /*public static void IzbrojUcionice()
        {
            List<Ucionica> ucioniceUnutarUstanove = new List<Ucionica>();

            foreach (var ustanova in PodaciUstanova.listaUstanova)
            {
                foreach (var ucionica in PodaciUcionica.listaUcionica)
                {
                    if (ucionica.UstanovaGdjeSeNalaziId.Equals(ustanova.SifraUstanove))
                    {
                        ucioniceUnutarUstanove.Add(ucionica);
                        int brojUcionica = ucioniceUnutarUstanove.Count();
                        ustanova.MaksimalanBrojUcionica = brojUcionica;
                    }
                }
            }

        }*/
    }
}
