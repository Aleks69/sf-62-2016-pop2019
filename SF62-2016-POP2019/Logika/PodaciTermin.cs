﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SF62_2016_POP2019.Entiteti;

namespace SF62_2016_POP2019.Logika
{
    class PodaciTermin
    {
        public static ObservableCollection<Termin> listaTermina { get; set; }

        /*public static void DodajTermine()
        {
            listaTermina = new ObservableCollection<Termin>();
            DateTime datetimenow = DateTime.Now;

            Termin termin1 = new Termin("1", new DateTime(datetimenow.Year, datetimenow.Month, datetimenow.Day, 12, 00, 00), new DateTime(datetimenow.Year, datetimenow.Month, datetimenow.Day, 14, 00, 00), EDaniUnedelji.PONEDELJAK, ETipNastave.Predavanja);
            Termin termin2 = new Termin("2", new DateTime(datetimenow.Year, datetimenow.Month, datetimenow.Day, 14, 00, 00), new DateTime(datetimenow.Year, datetimenow.Month, datetimenow.Day, 16, 00, 00), EDaniUnedelji.PONEDELJAK, ETipNastave.Predavanja);
            Termin termin3 = new Termin("3", new DateTime(datetimenow.Year, datetimenow.Month, datetimenow.Day, 16, 00, 00), new DateTime(datetimenow.Year, datetimenow.Month, datetimenow.Day, 18, 00, 00), EDaniUnedelji.PONEDELJAK, ETipNastave.Predavanja);
            termin1.VrijemeZauzecaPocetak.ToShortTimeString();
            termin1.VrijemeZauzecaKraj.ToString("HH:mm");
            listaTermina.Add(termin1);
            listaTermina.Add(termin2);
            listaTermina.Add(termin3);

        }*/

        public static void UcitajTermine()
        {
            listaTermina = new ObservableCollection<Termin>();

            using (SqlConnection conn = new SqlConnection(MainWindow.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from termini";
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                da.SelectCommand = command;
                da.Fill(ds, "Termini");
                foreach (DataRow row in ds.Tables["Termini"].Rows)
                {
                    int sifra = (int)row["IdTermin"];
                    DateTime vrijemeOd = (DateTime)row["VrijemeZauzecaPocetak"];
                    DateTime vrijemeDo = (DateTime)row["VrijemeZauzecaKraj"];
                    string daniuNedelji = (string)row["DaniUNedelji"];
                    string tipNastave = (string)row["TipNastave"];
                    int predavac = (int)row["ZaduzeniPredavacId"];
                    int ustanova = (int)row["UstanovaId"];
                    int ucionica = (int)row["UcionicaId"];
                    bool active = (bool)row["Active"];



                    if (tipNastave.ToLower().Equals(ETipNastave.Predavanja.ToString().ToLower()))
                    {
                        Termin termin = new Termin(sifra, vrijemeOd, vrijemeDo, ustanova, ucionica);
                        termin.TipNastave = ETipNastave.Predavanja;
                        termin.Active = active;
                        termin.ZaduzeniPredavacId = predavac;

                        if (daniuNedelji.ToLower() == EDaniUnedelji.PONEDELJAK.ToString().ToLower())
                        {
                            termin.DaniUNedelji = EDaniUnedelji.PONEDELJAK;
                            listaTermina.Add(termin);
                        }
                        else if (daniuNedelji.ToLower() == EDaniUnedelji.UTORAK.ToString().ToLower())
                        {
                            termin.DaniUNedelji = EDaniUnedelji.UTORAK;
                            listaTermina.Add(termin);
                        }
                        else if (daniuNedelji.ToLower() == EDaniUnedelji.SRIJEDA.ToString().ToLower())
                        {
                            termin.DaniUNedelji = EDaniUnedelji.SRIJEDA;
                            listaTermina.Add(termin);
                        }
                        else if (daniuNedelji.ToLower() == EDaniUnedelji.CETVRTAK.ToString().ToLower())
                        {
                            termin.DaniUNedelji = EDaniUnedelji.CETVRTAK;
                            listaTermina.Add(termin);
                        }
                        else if (daniuNedelji.ToLower() == EDaniUnedelji.PETAK.ToString().ToLower())
                        {
                            termin.DaniUNedelji = EDaniUnedelji.PETAK;
                            listaTermina.Add(termin);
                        }
                        else if (daniuNedelji.ToLower() == EDaniUnedelji.SUBOTA.ToString().ToLower())
                        {
                            termin.DaniUNedelji = EDaniUnedelji.SUBOTA;
                            listaTermina.Add(termin);
                        }
                        else if (daniuNedelji.ToLower() == EDaniUnedelji.NEDELJA.ToString().ToLower())
                        {
                            termin.DaniUNedelji = EDaniUnedelji.NEDELJA;
                            listaTermina.Add(termin);
                        }
                    }

                    if (tipNastave.ToLower().Equals(ETipNastave.Vjezbe.ToString().ToLower()))
                    {
                        Termin termin = new Termin(sifra, vrijemeOd, vrijemeDo, ustanova, ucionica);
                        termin.TipNastave = ETipNastave.Vjezbe;
                        termin.Active = active;
                        termin.ZaduzeniPredavacId = predavac;

                        if (daniuNedelji.ToLower() == EDaniUnedelji.PONEDELJAK.ToString().ToLower())
                        {
                            termin.DaniUNedelji = EDaniUnedelji.PONEDELJAK;
                            listaTermina.Add(termin);
                        }
                        else if (daniuNedelji.ToLower() == EDaniUnedelji.UTORAK.ToString().ToLower())
                        {
                            termin.DaniUNedelji = EDaniUnedelji.UTORAK;
                            listaTermina.Add(termin);
                        }
                        else if (daniuNedelji.ToLower() == EDaniUnedelji.SRIJEDA.ToString().ToLower())
                        {
                            termin.DaniUNedelji = EDaniUnedelji.SRIJEDA;
                            listaTermina.Add(termin);
                        }
                        else if (daniuNedelji.ToLower() == EDaniUnedelji.CETVRTAK.ToString().ToLower())
                        {
                            termin.DaniUNedelji = EDaniUnedelji.CETVRTAK;
                            listaTermina.Add(termin);
                        }
                        else if (daniuNedelji.ToLower() == EDaniUnedelji.PETAK.ToString().ToLower())
                        {
                            termin.DaniUNedelji = EDaniUnedelji.PETAK;
                            listaTermina.Add(termin);
                        }
                        else if (daniuNedelji.ToLower() == EDaniUnedelji.SUBOTA.ToString().ToLower())
                        {
                            termin.DaniUNedelji = EDaniUnedelji.SUBOTA;
                            listaTermina.Add(termin);
                        }
                        else if (daniuNedelji.ToLower() == EDaniUnedelji.NEDELJA.ToString().ToLower())
                        {
                            termin.DaniUNedelji = EDaniUnedelji.NEDELJA;
                            listaTermina.Add(termin);
                        }


                    }

                    foreach (var ust in PodaciUstanova.listaUstanova)
                    {
                        foreach (var termin in listaTermina)
                        {
                            if (ust.SifraUstanove.Equals(termin.UstanovaId))
                            {
                                termin.Ustanova = ust;
                            }
                        }
                    }

                    foreach (var pred in PodaciKorisnik.listaKorisnika)
                    {
                        foreach (var termin in listaTermina)
                        {
                            if (pred.Id.Equals(termin.ZaduzeniPredavacId))
                            {
                                termin.ZaduzeniPredavac = pred;
                            }
                        }
                    }

                    foreach (var uciona in PodaciUcionica.listaUcionica)
                    {
                        foreach (var termin in listaTermina)
                        {
                            if (uciona.IdUcionice.Equals(termin.UcionicaId))
                            {
                                termin.Ucionica = uciona;
                            }
                        }
                    }


                }
            }
        }

        public static void DodajTermine(Termin t)
        {
            using (SqlConnection connection = new SqlConnection(MainWindow.CONNECTION_STRING))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                command.CommandText =
                    @"INSERT INTO TERMINI(VrijemeZauzecaPocetak, VrijemeZauzecaKraj, DaniUNedelji, TipNastave, ZaduzeniPredavacId, UstanovaId, UcionicaId, Active) 
                    VALUES(@Pocetak, @Kraj, @Dan, @TipNastave, @ZaduzeniPredavacId, @UstanovaId, @UcionicaId, @Active) ";

                command.Parameters.Add(new SqlParameter("@Pocetak", t.VrijemeZauzecaPocetak));
                command.Parameters.Add(new SqlParameter("@Kraj", t.VrijemeZauzecaKraj));
                command.Parameters.Add(new SqlParameter("@Dan", t.DaniUNedelji.ToString()));
                command.Parameters.Add(new SqlParameter("@TipNastave", t.TipNastave.ToString()));
                command.Parameters.Add(new SqlParameter("@ZaduzeniPredavacId", t.ZaduzeniPredavacId));
                command.Parameters.Add(new SqlParameter("@UstanovaId", t.UstanovaId));
                command.Parameters.Add(new SqlParameter("@UcionicaId", t.UcionicaId));
                command.Parameters.Add(new SqlParameter("@Active", t.Active));


                command.ExecuteNonQuery();
            }
        }

        public static void IzmjeniTermin(Termin t)
        {
            using (SqlConnection conn = new SqlConnection(MainWindow.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();

                command.CommandText =
                    @"UPDATE TERMINI SET VrijemeZauzecaPocetak=@Pocetak, VrijemeZauzecaKraj=@Kraj, DaniUNedelji=@Dan, TipNastave=@TipNastave, 
                        ZaduzeniPredavacId=@ZaduzeniPredavacId, UstanovaId=@UstanovaId, UcionicaId=@UcionicaId WHERE IdTermin=@IdTermin";

                command.Parameters.Add(new SqlParameter("@Pocetak", t.VrijemeZauzecaPocetak));
                command.Parameters.Add(new SqlParameter("@Kraj", t.VrijemeZauzecaKraj));
                command.Parameters.Add(new SqlParameter("@Dan", t.DaniUNedelji.ToString()));
                command.Parameters.Add(new SqlParameter("@TipNastave", t.TipNastave.ToString()));
                command.Parameters.Add(new SqlParameter("@ZaduzeniPredavacId", t.ZaduzeniPredavacId));
                command.Parameters.Add(new SqlParameter("@UstanovaId", t.UstanovaId));
                command.Parameters.Add(new SqlParameter("@UcionicaId", t.UcionicaId));
                command.Parameters.Add(new SqlParameter("@Active", t.Active));
                command.Parameters.Add(new SqlParameter("IdTermin", t.IdTermin));

                command.ExecuteNonQuery();
            }
        }

        public static void IzbrisiTermin(Termin t)
        {
            using (SqlConnection connection = new SqlConnection(MainWindow.CONNECTION_STRING))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();

                command.CommandText = @"UPDATE TERMINI SET Active=0 WHERE IdTermin=@IdTermin";

                command.Parameters.Add(new SqlParameter("@Active", t.Active));
                command.Parameters.Add(new SqlParameter("@IdTermin", t.IdTermin));
                

                command.ExecuteNonQuery();
            }
        }

        public static Termin PretraziPoSifri(int id)
        {
            foreach (var termin in listaTermina)
            {
                if (termin.IdTermin.Equals(id))
                    return termin;
            }

            return null;
        }

        public static Termin PretraziPoIdu(int Id)
        {
            foreach (var termin in listaTermina)
            {
                if (termin.IdTermin.Equals(Id))
                    return termin;
            }

            return null;
        }

        public static TimeSpan IzracunajTrajanje(DateTime a, DateTime b)
        {
            TimeSpan vrijemeTrajanjTermina = a - b;
            return vrijemeTrajanjTermina;
        }
    }
}

