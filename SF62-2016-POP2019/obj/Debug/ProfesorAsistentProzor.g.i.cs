﻿#pragma checksum "..\..\ProfesorAsistentProzor.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "77F011C760863E82B16D70340A925B9B406D274B8147E8F8890EDF7E857619EA"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using SF62_2016_POP2019;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace SF62_2016_POP2019 {
    
    
    /// <summary>
    /// ProfesorAsistentProzor
    /// </summary>
    public partial class ProfesorAsistentProzor : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 10 "..\..\ProfesorAsistentProzor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabControl tabControl;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\ProfesorAsistentProzor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabItem tabItemProfil;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\ProfesorAsistentProzor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock textBlockPrezime;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\ProfesorAsistentProzor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock textBlockIme;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\ProfesorAsistentProzor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labelEmail;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\ProfesorAsistentProzor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labelUsername;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\ProfesorAsistentProzor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labelTipKor;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\ProfesorAsistentProzor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock textBlockKorIme;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\ProfesorAsistentProzor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock textBlockEmail;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\ProfesorAsistentProzor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock textBlockTip;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\ProfesorAsistentProzor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labelme;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\ProfesorAsistentProzor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\ProfesorAsistentProzor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabItem tabRaspored;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\ProfesorAsistentProzor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dataGrid;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\ProfesorAsistentProzor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labelRaspored;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\ProfesorAsistentProzor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonTermin;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\ProfesorAsistentProzor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabItem tabItemAsistenti;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\ProfesorAsistentProzor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dataGridListaAsistenata;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\ProfesorAsistentProzor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labelListaAsist;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\ProfesorAsistentProzor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonUkloni;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\ProfesorAsistentProzor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonDodaj;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\ProfesorAsistentProzor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_Copy;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/SF62-2016-POP2019;component/profesorasistentprozor.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\ProfesorAsistentProzor.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 8 "..\..\ProfesorAsistentProzor.xaml"
            ((SF62_2016_POP2019.ProfesorAsistentProzor)(target)).ContentRendered += new System.EventHandler(this.Window_ContentRendered);
            
            #line default
            #line hidden
            return;
            case 2:
            this.tabControl = ((System.Windows.Controls.TabControl)(target));
            return;
            case 3:
            this.tabItemProfil = ((System.Windows.Controls.TabItem)(target));
            return;
            case 4:
            this.textBlockPrezime = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 5:
            this.textBlockIme = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 6:
            this.labelEmail = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.labelUsername = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            this.labelTipKor = ((System.Windows.Controls.Label)(target));
            return;
            case 9:
            this.textBlockKorIme = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 10:
            this.textBlockEmail = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 11:
            this.textBlockTip = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 12:
            this.labelme = ((System.Windows.Controls.Label)(target));
            return;
            case 13:
            this.label = ((System.Windows.Controls.Label)(target));
            return;
            case 14:
            this.tabRaspored = ((System.Windows.Controls.TabItem)(target));
            
            #line 29 "..\..\ProfesorAsistentProzor.xaml"
            this.tabRaspored.GotFocus += new System.Windows.RoutedEventHandler(this.tabRaspored_GotFocus);
            
            #line default
            #line hidden
            return;
            case 15:
            this.dataGrid = ((System.Windows.Controls.DataGrid)(target));
            
            #line 31 "..\..\ProfesorAsistentProzor.xaml"
            this.dataGrid.AutoGeneratingColumn += new System.EventHandler<System.Windows.Controls.DataGridAutoGeneratingColumnEventArgs>(this.dataGrid_AutoGeneratingColumn);
            
            #line default
            #line hidden
            return;
            case 16:
            this.labelRaspored = ((System.Windows.Controls.Label)(target));
            return;
            case 17:
            this.buttonTermin = ((System.Windows.Controls.Button)(target));
            
            #line 33 "..\..\ProfesorAsistentProzor.xaml"
            this.buttonTermin.Click += new System.Windows.RoutedEventHandler(this.buttonTermin_Click);
            
            #line default
            #line hidden
            return;
            case 18:
            this.tabItemAsistenti = ((System.Windows.Controls.TabItem)(target));
            
            #line 36 "..\..\ProfesorAsistentProzor.xaml"
            this.tabItemAsistenti.GotFocus += new System.Windows.RoutedEventHandler(this.tabItemAsistenti_GotFocus);
            
            #line default
            #line hidden
            return;
            case 19:
            this.dataGridListaAsistenata = ((System.Windows.Controls.DataGrid)(target));
            
            #line 43 "..\..\ProfesorAsistentProzor.xaml"
            this.dataGridListaAsistenata.AutoGeneratingColumn += new System.EventHandler<System.Windows.Controls.DataGridAutoGeneratingColumnEventArgs>(this.dataGridListaAsistenata_AutoGeneratingColumn);
            
            #line default
            #line hidden
            return;
            case 20:
            this.labelListaAsist = ((System.Windows.Controls.Label)(target));
            return;
            case 21:
            this.buttonUkloni = ((System.Windows.Controls.Button)(target));
            
            #line 45 "..\..\ProfesorAsistentProzor.xaml"
            this.buttonUkloni.Click += new System.Windows.RoutedEventHandler(this.buttonUkloni_Click);
            
            #line default
            #line hidden
            return;
            case 22:
            this.buttonDodaj = ((System.Windows.Controls.Button)(target));
            
            #line 46 "..\..\ProfesorAsistentProzor.xaml"
            this.buttonDodaj.Click += new System.Windows.RoutedEventHandler(this.buttonDodaj_Click);
            
            #line default
            #line hidden
            return;
            case 23:
            this.label_Copy = ((System.Windows.Controls.Label)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

