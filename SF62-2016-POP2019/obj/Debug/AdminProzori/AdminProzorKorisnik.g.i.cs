﻿#pragma checksum "..\..\..\AdminProzori\AdminProzorKorisnik.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "C6EE0E75272562F09E6CFF1B1542B09EDA1D4198A78C4BA1354DCE896B3CFE0B"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using SF62_2016_POP2019;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace SF62_2016_POP2019 {
    
    
    /// <summary>
    /// AdminProzor
    /// </summary>
    public partial class AdminProzor : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 16 "..\..\..\AdminProzori\AdminProzorKorisnik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonAdd;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\..\AdminProzori\AdminProzorKorisnik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonDelete;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\..\AdminProzori\AdminProzorKorisnik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonEdit;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\..\AdminProzori\AdminProzorKorisnik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox textBoxSearchKorIme;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\..\AdminProzori\AdminProzorKorisnik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonSearch;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\..\AdminProzori\AdminProzorKorisnik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dataGridKorisnici;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\..\AdminProzori\AdminProzorKorisnik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton radioButtonAdmin;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\..\AdminProzori\AdminProzorKorisnik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton radioButtonProfesor;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\..\AdminProzori\AdminProzorKorisnik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton radioButtonAsistent;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\..\AdminProzori\AdminProzorKorisnik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox textBoxSearchPrezime;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\..\AdminProzori\AdminProzorKorisnik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label KorImeLabel;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\..\AdminProzori\AdminProzorKorisnik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_Copy;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\..\AdminProzori\AdminProzorKorisnik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox textBoxSearchIme;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\..\AdminProzori\AdminProzorKorisnik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox textBoxSearchEmail;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\..\AdminProzori\AdminProzorKorisnik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_Copy1;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\..\AdminProzori\AdminProzorKorisnik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_Copy2;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\..\AdminProzori\AdminProzorKorisnik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_Copy3;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\..\AdminProzori\AdminProzorKorisnik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox comboBoxTip;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\..\AdminProzori\AdminProzorKorisnik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button button;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\..\AdminProzori\AdminProzorKorisnik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button button_sort;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\..\AdminProzori\AdminProzorKorisnik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton radioButtonkorIme;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\..\AdminProzori\AdminProzorKorisnik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton radioButtonIme;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\..\AdminProzori\AdminProzorKorisnik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton radioButton_Prezime;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\..\AdminProzori\AdminProzorKorisnik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton radioButtonEmail;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\..\AdminProzori\AdminProzorKorisnik.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton radioButtonTipKor;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/SF62-2016-POP2019;component/adminprozori/adminprozorkorisnik.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\AdminProzori\AdminProzorKorisnik.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 8 "..\..\..\AdminProzori\AdminProzorKorisnik.xaml"
            ((SF62_2016_POP2019.AdminProzor)(target)).ContentRendered += new System.EventHandler(this.Window_ContentRendered);
            
            #line default
            #line hidden
            return;
            case 2:
            this.buttonAdd = ((System.Windows.Controls.Button)(target));
            
            #line 16 "..\..\..\AdminProzori\AdminProzorKorisnik.xaml"
            this.buttonAdd.Click += new System.Windows.RoutedEventHandler(this.buttonAdd_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.buttonDelete = ((System.Windows.Controls.Button)(target));
            
            #line 17 "..\..\..\AdminProzori\AdminProzorKorisnik.xaml"
            this.buttonDelete.Click += new System.Windows.RoutedEventHandler(this.buttonDelete_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.buttonEdit = ((System.Windows.Controls.Button)(target));
            
            #line 18 "..\..\..\AdminProzori\AdminProzorKorisnik.xaml"
            this.buttonEdit.Click += new System.Windows.RoutedEventHandler(this.buttonEdit_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.textBoxSearchKorIme = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.buttonSearch = ((System.Windows.Controls.Button)(target));
            
            #line 20 "..\..\..\AdminProzori\AdminProzorKorisnik.xaml"
            this.buttonSearch.Click += new System.Windows.RoutedEventHandler(this.buttonSearch_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.dataGridKorisnici = ((System.Windows.Controls.DataGrid)(target));
            
            #line 21 "..\..\..\AdminProzori\AdminProzorKorisnik.xaml"
            this.dataGridKorisnici.AutoGeneratingColumn += new System.EventHandler<System.Windows.Controls.DataGridAutoGeneratingColumnEventArgs>(this.dataGridKorisnici_AutoGeneratingColumn);
            
            #line default
            #line hidden
            return;
            case 8:
            this.radioButtonAdmin = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 9:
            this.radioButtonProfesor = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 10:
            this.radioButtonAsistent = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 11:
            this.textBoxSearchPrezime = ((System.Windows.Controls.TextBox)(target));
            return;
            case 12:
            this.KorImeLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 13:
            this.label_Copy = ((System.Windows.Controls.Label)(target));
            return;
            case 14:
            this.textBoxSearchIme = ((System.Windows.Controls.TextBox)(target));
            return;
            case 15:
            this.textBoxSearchEmail = ((System.Windows.Controls.TextBox)(target));
            return;
            case 16:
            this.label_Copy1 = ((System.Windows.Controls.Label)(target));
            return;
            case 17:
            this.label_Copy2 = ((System.Windows.Controls.Label)(target));
            return;
            case 18:
            this.label_Copy3 = ((System.Windows.Controls.Label)(target));
            return;
            case 19:
            this.comboBoxTip = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 20:
            this.button = ((System.Windows.Controls.Button)(target));
            
            #line 34 "..\..\..\AdminProzori\AdminProzorKorisnik.xaml"
            this.button.Click += new System.Windows.RoutedEventHandler(this.button_Click);
            
            #line default
            #line hidden
            return;
            case 21:
            this.button_sort = ((System.Windows.Controls.Button)(target));
            
            #line 35 "..\..\..\AdminProzori\AdminProzorKorisnik.xaml"
            this.button_sort.Click += new System.Windows.RoutedEventHandler(this.button_sort_Click);
            
            #line default
            #line hidden
            return;
            case 22:
            this.radioButtonkorIme = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 23:
            this.radioButtonIme = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 24:
            this.radioButton_Prezime = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 25:
            this.radioButtonEmail = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 26:
            this.radioButtonTipKor = ((System.Windows.Controls.RadioButton)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

