﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF62_2016_POP2019.Entiteti
{
    public class Asistent : Korisnik
    {
        private Profesor _dodjeljeniProfesor;
        public Profesor DodjeljeniProfesor
        {
            get { return _dodjeljeniProfesor; }
            set { _dodjeljeniProfesor = value; }
        }

        private int _idDodjeljenogProfesora;

        public int IdDodjeljenogProfesora
        {
            get { return _idDodjeljenogProfesora; }
            set { _idDodjeljenogProfesora = value; }
        }

        private List<Termin> _termini = new List<Termin>();
        public List<Termin> Termini
        {
            get { return _termini; }
            set { _termini = value; }
        }

        private Ustanova _ustanovaZaposlenja;
        public Ustanova UstanovaZaposlenja
        {
            get { return _ustanovaZaposlenja; }
            set { _ustanovaZaposlenja = value; }
        }

        private int _ustanovaZaposlenjaId;
        public int UstanovaZaposlenjaId
        {
            get { return _ustanovaZaposlenjaId; }
            set { _ustanovaZaposlenjaId = value; }
        }

        public Asistent(string ime, string prezime, string korIme, string email) : base(ime, prezime, korIme, email)
        {
            TipKorisnika = ETipKorisnika.ASISTENT;
            //Active = true;
        }

        public Asistent(string ime, string prezime, string korIme, string email,string lozinka) : base(ime, prezime, korIme, email, lozinka)
        {
            TipKorisnika = ETipKorisnika.ASISTENT;
            //Active = true;
        }

        public Asistent(string ime, string prezime, string korIme, string email, string lozinka, Profesor dodjeljeniProfesor, Ustanova ustanovaZaposlenja) : base(ime, prezime, korIme, email, lozinka)
        {
            TipKorisnika = ETipKorisnika.ASISTENT;
           // Active = true;
            this.DodjeljeniProfesor = dodjeljeniProfesor;
            this.UstanovaZaposlenja = ustanovaZaposlenja;
        }


        public Asistent(int id,string ime, string prezime, string korIme, string email, string lozinka) : base(id, ime, prezime, korIme, email, lozinka)
        {
            TipKorisnika = ETipKorisnika.ASISTENT;
            //Active = true;
        }
        public Asistent()
        {
            KorisnickoIme = "";
            TipKorisnika = ETipKorisnika.ASISTENT;
          //  Active = true;

        }

        public override Korisnik Clone()
        {
            Asistent asistent = new Asistent(Id,Ime,Prezime,KorisnickoIme,Email, Lozinka);
            return asistent;
        }

        public override string ToString()
        {
            return this.Ime + " " + this.Prezime;
        }
    }
}
