﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace SF62_2016_POP2019.Entiteti
{
    public class Termin /*: INotifyPropertyChanged*/
    {
        /*private int _idTermin;

        public int IdTermin
        {
            get { return _idTermin; }
            set { _idTermin = value; }
        }*/
        private int _idTermin;
        public int IdTermin
        {
            get { return _idTermin; }
            set { _idTermin = value; } /*OnPropertyChanged("IdTermin"); */
        }

        private DateTime _vrijemeZauzecaPocetak;
        public DateTime VrijemeZauzecaPocetak
        {
            get { return _vrijemeZauzecaPocetak; }
            set { _vrijemeZauzecaPocetak = value; /*OnPropertyChanged("VrijemeZauzecaPocetak"); */}
        }

        private DateTime _vrijemeZauzecaKraj;
        public DateTime VrijemeZauzecaKraj
        {
            get { return _vrijemeZauzecaKraj; }
            set { _vrijemeZauzecaKraj = value; /*OnPropertyChanged("VrijemeZauzecaPocetak");*/}
        }

        /*private TimeSpan _trajanjeTermina;
        public TimeSpan TrajanjeTermina
        {
            get { return _trajanjeTermina; }
            set { _trajanjeTermina = value; OnPropertyChanged("TrajanjeTermina"); }
        }*/

        private EDaniUnedelji _daniUNedelji;
        public EDaniUnedelji DaniUNedelji
        {
            get { return _daniUNedelji; }
            set { _daniUNedelji = value; /*OnPropertyChanged("DaniUNedelji");*/}
        }

        private ETipNastave _tipNastave;
        public ETipNastave TipNastave
        {
            get { return _tipNastave; }
            set { _tipNastave = value; /*OnPropertyChanged("TipNastave");*/ }
        }

        private bool _active = true;
        public bool Active
        {
            get { return _active; }
            set { _active = value; /*OnPropertyChanged("Active");*/}
        }

        private Korisnik _zaduzeniPredavac;
        public Korisnik ZaduzeniPredavac
        {
            get { return _zaduzeniPredavac; }
            set { _zaduzeniPredavac = value; /*OnPropertyChanged("ZaduzeniPredavac");*/}
        }

        private int _zaduzeniPredavacId;
        public int ZaduzeniPredavacId
        {
            get { return _zaduzeniPredavacId; }
            set { _zaduzeniPredavacId = value; /*OnPropertyChanged("ZaduzeniPredavac");*/}
        }

        private Ustanova _ustanova;

        public Ustanova Ustanova
        {
            get { return _ustanova; }
            set { _ustanova = value; /*OnPropertyChanged("Ustanova");*/}
        }

        private int _ustanovaId;

        public int UstanovaId
        {
            get { return _ustanovaId; }
            set { _ustanovaId = value; /*OnPropertyChanged("Ustanova");*/}
        }

        private Ucionica _ucnionica;

        public Ucionica Ucionica
        {
            get { return _ucnionica; }
            set { _ucnionica = value; /*OnPropertyChanged("Ucionica");*/}
        }

        private int _ucnionicaId;

        public int UcionicaId
        {
            get { return _ucnionicaId; }
            set { _ucnionicaId = value; /*OnPropertyChanged("Ucionica");*/}
        }

        public Termin(int sifra, DateTime VrijemeOd, DateTime VrijemeDo, EDaniUnedelji dani, ETipNastave tipNastave, Ustanova ustanova, Ucionica ucionica)
        {
            this.IdTermin = sifra;
            this.VrijemeZauzecaPocetak = VrijemeOd;
            this.VrijemeZauzecaKraj = VrijemeDo;
            this.DaniUNedelji = dani;
            this.TipNastave = tipNastave;
            this.Ustanova = ustanova;
            this.Ucionica = ucionica;
        }

        public Termin(int sifra, DateTime VrijemeOd, DateTime VrijemeDo, int ustanovaID, int ucionicaID)
        {
            this.IdTermin = sifra;
            this.VrijemeZauzecaPocetak = VrijemeOd;
            this.VrijemeZauzecaKraj = VrijemeDo;
            this.UstanovaId = ustanovaID;
            this.UcionicaId = ucionicaID;
        }

        public Termin(int sifra, DateTime VrijemeOd, DateTime VrijemeDo, EDaniUnedelji dani, ETipNastave tipNastave)
        {
            this.IdTermin = sifra;
            this.VrijemeZauzecaPocetak = VrijemeOd;
            this.VrijemeZauzecaKraj = VrijemeDo;
            this.DaniUNedelji = dani;
            this.TipNastave = tipNastave;
        }

        public Termin(int sifra, DateTime VrijemeOd, DateTime VrijemeDo, EDaniUnedelji dani, ETipNastave tipNastave, bool active)
        {
            this.IdTermin = sifra;
            this.VrijemeZauzecaPocetak = VrijemeOd;
            this.VrijemeZauzecaKraj = VrijemeDo;
            this.DaniUNedelji = dani;
            this.TipNastave = tipNastave;
            this.Active = active;
        }

        public Termin()
        {
            this.IdTermin = 0;
        }

        public virtual Termin Clone()
        {
            Termin termin = new Termin(IdTermin, VrijemeZauzecaPocetak, VrijemeZauzecaKraj, DaniUNedelji, TipNastave, Active);
            return termin;
        }

        public override string ToString()
        {
            return VrijemeZauzecaPocetak.ToShortTimeString() + " - " + VrijemeZauzecaKraj.ToShortTimeString();
        }

        public string this[string propertyName]
        {
            get
            {
                switch (propertyName)
                {
                    case "VrijemeZauzecaPocetak":
                        if (Convert.ToString(VrijemeZauzecaPocetak).Equals(string.Empty))
                            return "Ovo polje je obavezno!";
                        break;
                }
                return null;
            }
        }
        /*public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if(PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }*/
    }
}
