﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF62_2016_POP2019.Entiteti
{
    public class Ucionica : INotifyPropertyChanged
    {

        /* private int _idUcionice;

        public int IdUcionice
        {
            get { return _idUcionice; }
            set { _idUcionice = value; }
        }*/
        private int _idUcionice;

        public int IdUcionice
        {
            get { return _idUcionice; }
            set
            {
                _idUcionice = value;
                OnPropertyChanged("IdUcionice");
            }
        }

        private string _brojUcionice;

        public string BrojUcionice
        {
            get { return _brojUcionice; }
            set
            {
                _brojUcionice = value;
                OnPropertyChanged("BrojUcionice");
            }
        }

        private int _brojMjesta;

        public int BrojMjesta
        {
            get { return _brojMjesta; }
            set
            {
                _brojMjesta = value;
                OnPropertyChanged("BrojMjesta");
            }
        }

        private ETipUcionice _tipUcionice;

        public ETipUcionice TipUcionice
        {
            get { return _tipUcionice; }
            set
            {
                _tipUcionice = value;
                OnPropertyChanged("TipUcionice");
            }
        }

        private bool _active = true;

        public bool Active
        {
            get { return _active; }
            set
            {
                _active = value;
                OnPropertyChanged("Active");
            }
        }

        private Ustanova _ustanovaGdjeSeNalazi;

        public Ustanova UstanovaGdjeSeNalazi
        {
            get { return _ustanovaGdjeSeNalazi; }
            set
            {
                _ustanovaGdjeSeNalazi = value;
                OnPropertyChanged("UstanovaGdjeSeNalazi");
            }
        }

        private int _ustanovaGdjeSeNalaziId;

        public int UstanovaGdjeSeNalaziId
        {
            get { return _ustanovaGdjeSeNalaziId; }
            set
            {
                _ustanovaGdjeSeNalaziId = value;
                OnPropertyChanged("UstanovaGdjeSeNalaziId");
            }
        }

        public Ucionica(int id, string brojUcionice, int brojMjesta, ETipUcionice tipUcionice)
        {
            this.IdUcionice = id;
            this.BrojUcionice = brojUcionice;
            this.BrojMjesta = brojMjesta;
            this.TipUcionice = tipUcionice;
        }

        public Ucionica(int id, string brojUcionice, int brojMjesta, ETipUcionice tipUcionice, bool active)
        {
            this.IdUcionice = id;
            this.BrojUcionice = brojUcionice;
            this.BrojMjesta = brojMjesta;
            this.TipUcionice = tipUcionice;
            this.Active = active;
        }

        public Ucionica()
        {
            this.IdUcionice = 0;
        }

        public Ucionica Clone()
        {
            Ucionica ucnionica = new Ucionica(IdUcionice, BrojUcionice, BrojMjesta, TipUcionice, Active);
            this.Active = true;
            return ucnionica;
        }

        public override string ToString()
        {
            return "Ucionica " + this.BrojUcionice;
        }


        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public string Error
        {
            get { return null; }
        }

        public string this[string propertyName]
        {
            get
            {
                switch (propertyName)
                {
                    case "BrojUcionice":
                        if (BrojUcionice.Equals(string.Empty))
                            return "Ovo polje je obavezno!";
                        break;
                }
                return null;
            }
        }
    }
}
