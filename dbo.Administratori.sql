﻿CREATE TABLE [dbo].[Administratori] (
    [IdAdmin] INT NOT NULL,
    PRIMARY KEY CLUSTERED ([IdAdmin] ASC),
    CONSTRAINT [FK_Admin] FOREIGN KEY ([IdAdmin]) REFERENCES [dbo].[Korisnici] ([IdKorisnik])
);

