﻿SET IDENTITY_INSERT [dbo].[Korisnici] ON
INSERT INTO [dbo].[Korisnici] ([IdKorisnik], [Ime], [Prezime], [KorisnickoIme], [Email], [TipKorisnika], [Lozinka], [Active]) VALUES (1, N'Pero', N'Peric', N'Pero69', N'Pero69@gmail.com', N'ADMIN', N'asdf123', 1)
INSERT INTO [dbo].[Korisnici] ([IdKorisnik], [Ime], [Prezime], [KorisnickoIme], [Email], [TipKorisnika], [Lozinka], [Active]) VALUES (2, N'Dragan', N'Stanisic', N'DraganS', N'Gandra88@hotmail.com', N'ADMIN', N'asdf123', 1)
INSERT INTO [dbo].[Korisnici] ([IdKorisnik], [Ime], [Prezime], [KorisnickoIme], [Email], [TipKorisnika], [Lozinka], [Active]) VALUES (3, N'Djuro', N'Djuric', N'Djuro43', N'Djuro43@hotmail.com', N'PROFESOR', N'asdf123', 1)
INSERT INTO [dbo].[Korisnici] ([IdKorisnik], [Ime], [Prezime], [KorisnickoIme], [Email], [TipKorisnika], [Lozinka], [Active]) VALUES (4, N'Gavro', N'Garic', N'Gavro22', N'Gavro22@ninjamail.net', N'PROFESOR', N'asdf123', 1)
INSERT INTO [dbo].[Korisnici] ([IdKorisnik], [Ime], [Prezime], [KorisnickoIme], [Email], [TipKorisnika], [Lozinka], [Active]) VALUES (5, N'Mario', N'Maric', N'MMario', N'MMario@hotmail.com', N'PROFESOR', N'asdf123', 1)
INSERT INTO [dbo].[Korisnici] ([IdKorisnik], [Ime], [Prezime], [KorisnickoIme], [Email], [TipKorisnika], [Lozinka], [Active]) VALUES (6, N'Drazen', N'Drazic', N'Drazo69', N'CicaDraza@gmail.net', N'PROFESOR', N'asdf123', 1)
INSERT INTO [dbo].[Korisnici] ([IdKorisnik], [Ime], [Prezime], [KorisnickoIme], [Email], [TipKorisnika], [Lozinka], [Active]) VALUES (7, N'Dimitrije', N'Tucovic', N'Dimke', N'DT1991@gmail.net', N'ASISTENT', N'asdf123', 1)
INSERT INTO [dbo].[Korisnici] ([IdKorisnik], [Ime], [Prezime], [KorisnickoIme], [Email], [TipKorisnika], [Lozinka], [Active]) VALUES (8, N'Petar', N'Petrovic', N'Petar96', N'Petar96@hotmail.net', N'ASISTENT', N'asdf123', 1)
INSERT INTO [dbo].[Korisnici] ([IdKorisnik], [Ime], [Prezime], [KorisnickoIme], [Email], [TipKorisnika], [Lozinka], [Active]) VALUES (9, N'Nikola', N'Djuricko', N'Nidza22', N'Niki234@gmail.com', N'ASISTENT', N'asdf123', 1)
SET IDENTITY_INSERT [dbo].[Korisnici] OFF
