﻿CREATE TABLE [dbo].[Ustanove] (
    [IdUstanova]             INT           IDENTITY (1, 1) NOT NULL,
    [Naziv]                  NVARCHAR (50) NULL,
    [Lokacija]               NVARCHAR (50) NULL,
    [Active]                 BIT           NULL,
    [MaksimalanBrojUcionica] INT           NULL,
    PRIMARY KEY CLUSTERED ([IdUstanova] ASC)
);

